package week09;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SimpleForm extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// -----------------------------------------------
		// -----------------------------------------------

				// 1. Create & configure user interface controls

				// -----------------------------------------------

				

				// name label

				

				Label nameLabel = new Label("Enter your First Name");

				Label name1Label = new Label("Enter your Last Name");

				Label emailLabel = new Label("Enter your Email Id");

				Label psswrdLabel = new Label("Enter your Password");

				// name TextBox

				TextField nameTextBox = new TextField();

				TextField name1TextBox = new TextField();

				TextField emailTextBox = new TextField();

				TextField psswrdTextBox = new TextField();

				// button

				Button goButton = new Button();

				goButton.setText("SIGN IN");

				

				goButton.setOnAction(new EventHandler<ActionEvent>() {

				    @Override

				    public void handle(ActionEvent e) {

				        // Logic for what should happen when you push button

		               System.out.println("THANK YOU!!!!!!!");

				    }

				});

				

				// -----------------------------------------------

				// 2. Make a layout manager

				// -----------------------------------------------

				VBox root = new VBox();

				//HBox root = new HBox();

				// -----------------------------------------------

				// 3. Add controls to the layout manager

				// -----------------------------------------------

				

				// add controls in the same order you want them to appear

				

				root.getChildren().add(nameLabel);

				root.getChildren().add(nameTextBox);

				root.getChildren().add(name1Label);

				root.getChildren().add(name1TextBox);

				root.getChildren().add(emailLabel);

				root.getChildren().add(emailTextBox);

				root.getChildren().add(psswrdLabel);

				root.getChildren().add(psswrdTextBox);

				root.getChildren().add(goButton);

				// -----------------------------------------------

				// 4. Add layout manager to scene

				// 5. Add scene to a stage

				// -----------------------------------------------

				// set the width & height of application to (300 x 250);

				primaryStage.setScene(new Scene(root, 450, 500));

				// setting the title bar of the application

				primaryStage.setTitle("Example 01");

				// -----------------------------------------------

				// 6. Show the application

				// -----------------------------------------------

				primaryStage.show();	

			}
}
